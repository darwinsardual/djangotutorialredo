from django.apps import AppConfig


class PollsredoConfig(AppConfig):
    name = 'pollsredo'
