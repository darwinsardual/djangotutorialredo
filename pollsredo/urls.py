from django.urls import path
from . import views

app_name = 'pollsredo'

urlpatterns = [
    path('', views.PollsIndexView.as_view(), name='polls'),
    path('<int:pk>/', views.PollsDetailsView.as_view(), name='details'),
    path('<int:pk>/results/', views.PollsResultsView.as_view(), name='results'),
    path('<int:pk>/vote', views.vote, name='vote')
]
