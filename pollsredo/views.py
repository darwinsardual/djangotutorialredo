from django.shortcuts import render
from django.views import generic
from django.http import HttpResponse, HttpResponseRedirect
from .models import Question
from django.shortcuts import get_object_or_404
from django.urls import reverse
from django.db.models import F

# Create your views here.

class PollsIndexView(generic.ListView):
    model = Question
    template_name = 'pollsredo/polls.html'

    def get_queryset(self):
        return Question.objects.order_by('-publish_date')[:5]

class PollsDetailsView(generic.DetailView):
    model = Question
    template_name = 'pollsredo/details.html'

    #get_queryset

class PollsResultsView(generic.DetailView):
    model = Question
    template_name = 'pollsredo/results.html'

def vote(request, pk):
    question = get_object_or_404(Question, pk=pk)

    try:
        choice = question.choice_set.get(pk=request.POST['choice'])
    except (KeyError, Choice.DoesNotExist):
        return render(request, 'pollsredo/details.html', {
            'question': question,
            'error': 'There was an error processing the request'
        })

    else:
        choice.num_votes = F('num_votes') + 1
        choice.save()

        return HttpResponseRedirect(reverse('pollsredo:results', args=(pk,)))
