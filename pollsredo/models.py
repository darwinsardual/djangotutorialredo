import datetime

from django.db import models
from django.utils import timezone


# Create your models here.
class Question(models.Model):
    text = models.CharField(max_length=100)
    publish_date = models.DateTimeField('publish date')

    def __str__(self):
        return self.text

    def recently_published(self):
        now = timezone.now()
        return now - datetime.timedelta(days=1) <= self.publish_date and self.publish_date < now

    recently_published.admin_order_field = 'pub_date'
    recently_published.boolean = True
    recently_published.short_description = 'Published recently?'

class Choice(models.Model):
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    text = models.CharField(max_length=100)
    num_votes = models.PositiveSmallIntegerField(default=0)

    def __str__(self):
        return self.text
