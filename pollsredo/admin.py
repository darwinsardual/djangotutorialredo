from django.contrib import admin
from .models import Question, Choice

# Register your models here.

class ChoiceInline(admin.StackedInline):
    model = Choice
    extra = 2

class QuestionAdmin(admin.ModelAdmin):
    fieldsets = [
        (None, {'fields': ['text']}),
        ('Date', {'fields': ['publish_date']})
    ]

    inlines = [ChoiceInline]
    list_display = ('text', 'publish_date', 'recently_published')
    list_filter = ('publish_date',)
    search_fields = ('text',)

admin.site.register(Question, QuestionAdmin)
